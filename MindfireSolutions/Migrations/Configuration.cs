namespace MindfireSolutions.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MindfireSolutions.UserModelContext.UserAccountContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MindfireSolutions.UserModelContext.UserAccountContext context)
        {
            List<UserContactType> addContact = new List<UserContactType>();
            addContact.Add(new UserContactType() { Type = "alternetMobile" });
            addContact.Add(new UserContactType() { Type = "home" });
            addContact.Add(new UserContactType() { Type = "office" });
            addContact.Add(new UserContactType() { Type = "fax" });

            context.UserContactDetails.AddRange(addContact);

            List<Role> addRole = new List<Role>();
            addRole.Add(new Role() { RoleType = "Admin" });
            addRole.Add(new Role() { RoleType = "User" });

            context.RoleDetails.AddRange(addRole);
        }
    }
}
