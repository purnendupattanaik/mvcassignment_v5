namespace MindfireSolutions.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Account : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ErrorAndExceptionClasses",
                c => new
                    {
                        ErrorId = c.Int(nullable: false, identity: true),
                        ErrorName = c.String(),
                        Time = c.String(),
                        UserErrorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ErrorId)
                .ForeignKey("dbo.Users", t => t.UserErrorId, cascadeDelete: true)
                .Index(t => t.UserErrorId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        Address = c.String(),
                        IsEmailVarified = c.Boolean(nullable: false),
                        ActivationCode = c.Guid(nullable: false),
                        Mobile = c.String(),
                        ImageUrl = c.String(),
                        AlternetText = c.String(),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.Int(nullable: false, identity: true),
                        RoleType = c.String(),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.UserContacts",
                c => new
                    {
                        HoldsId = c.Int(nullable: false, identity: true),
                        ContactNumber = c.String(),
                        ContactId = c.Int(nullable: false),
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.HoldsId)
                .ForeignKey("dbo.UserContactTypes", t => t.ContactId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.Id, cascadeDelete: true)
                .Index(t => t.ContactId)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.UserContactTypes",
                c => new
                    {
                        ContactId = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ContactId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserContacts", "Id", "dbo.Users");
            DropForeignKey("dbo.UserContacts", "ContactId", "dbo.UserContactTypes");
            DropForeignKey("dbo.ErrorAndExceptionClasses", "UserErrorId", "dbo.Users");
            DropForeignKey("dbo.Users", "RoleId", "dbo.Roles");
            DropIndex("dbo.UserContacts", new[] { "Id" });
            DropIndex("dbo.UserContacts", new[] { "ContactId" });
            DropIndex("dbo.Users", new[] { "RoleId" });
            DropIndex("dbo.ErrorAndExceptionClasses", new[] { "UserErrorId" });
            DropTable("dbo.UserContactTypes");
            DropTable("dbo.UserContacts");
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
            DropTable("dbo.ErrorAndExceptionClasses");
        }
    }
}
