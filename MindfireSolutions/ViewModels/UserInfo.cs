﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MindfireSolutions.ViewModels
{
    public class UserInfo
    {
        public int Id;

        [DisplayName("First Name *")]
        [Required(ErrorMessage = "You must provide a first name")]
        [RegularExpression("^([a-zA-Z]+)$", ErrorMessage = "Use only alphabets")]
        public string FirstName { get; set; }

        [DisplayName("Last Name *")]
        [Required(ErrorMessage = "You must provide a last name")]
        [RegularExpression("^([a-zA-Z]+)$", ErrorMessage = "Use only alphabets")]
        public string LastName { get; set; }

        [DisplayName("Address *")]
        [Required(ErrorMessage = "You must provide a address")]
        public string Address { get; set; }
    }
}