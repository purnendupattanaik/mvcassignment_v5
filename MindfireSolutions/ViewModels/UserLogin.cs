﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MindfireSolutions.ViewModels
{
    public class UserLogin
    {
        [Required(ErrorMessage = "Invalid email or password")]
        [Display(Name = "Email *")]
        [EmailAddress]
        public string EmailId { get; set; }

        [Required(ErrorMessage = "Invalid email or password")]
        [DataType(DataType.Password)]
        [Display(Name = "Password *")]
        public string Password { get; set; }
    }
}