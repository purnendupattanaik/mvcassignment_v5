﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MindfireSolutions.ViewModels
{
    public class PasswordReset
    {
        [Required]
        [EmailAddress]
        public string EmailId { get; set; }
    }
}