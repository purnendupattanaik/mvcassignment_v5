﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MindfireSolutions.Models
{
    public class UserContact
    {
        [Key]
        public int HoldsId { get; set; }

        public string ContactNumber { get; set; }

        [ForeignKey("UserContactData")]
        public int ContactId { get; set; }
        public UserContactType UserContactData { get; set; }

        [ForeignKey("UserData")]
        public int Id { get; set; }
        public User UserData { get; set; }

    }
}