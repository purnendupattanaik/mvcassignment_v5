﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MindfireSolutions.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public bool IsEmailVarified { get; set; }
        public System.Guid ActivationCode { get; set; }
        public string Mobile { get; set; }
        [DataType(DataType.ImageUrl)]
        public string ImageUrl { get; set; }
        public string AlternetText { get; set; }

        [ForeignKey("RoleData")]
        public int RoleId { get; set; }
        public Role RoleData { get; set; }

    }

}