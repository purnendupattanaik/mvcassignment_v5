﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MindfireSolutions.Models
{
    public class ErrorAndExceptionClass
    {
        [Key]
        public int ErrorId { get; set; }
        public string ErrorName { get; set; }
        public string Time { get; set; }

        [ForeignKey("UserErrorData")]
        public int UserErrorId { get; set; }
        public User UserErrorData { get; set; }
    }
}