﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using MindfireSolutions.ViewModels;
using MindfireSolutions.Models;
using MindfireSolutions.UserModelContext;
using System.Data;
using System.Data.Entity;
using MindfireSolutions.CustomFilters;
using MindfireSolutions.BAL;
using System.Web.Security;
using MindfireSolutions.BALInterface;
namespace MindfireSolutions.Controllers
{
    public class HomeController : Controller
    {
        private IUserLoginBAL userLoginBAL;

        public HomeController(IUserLoginBAL _userLoginBAL)
        {
            userLoginBAL = _userLoginBAL;
        }
        // GET: Home
        /// <summary>This action is home index
        public ActionResult Index()

        {
            return View();
        }
        /// <summary>This action is home index
        public ActionResult ErrorPage()
        {
            return View();
        }

        /// <summary>This action is display all user's details
        [Authorize(Roles = "1")]
        [HttpGet]
        public ActionResult Details()
        {
            try
            {
                using (UserAccountContext userAccount = new UserAccountContext())
                {
                    return View(userAccount.UserDetails.ToList<User>());
                }
            }
            catch (Exception exception)
            {
                return RedirectToAction("ErrorPage", "Home");
            }
        }

        /// <summary>This action is display specific user's details
        [CustomAuth]
        public ActionResult UserDetails(int? id)
        {
            try
            {
                if (id != null)
                {
                    return View(userLoginBAL.ShowUserData(id));
                }
                return RedirectToAction("ErrorPage", "Home");
            }
            catch (Exception exception)
            {
                //ErrorBAL.ErrorDetailsBAL(id, exception);
                return RedirectToAction("ErrorPage", "Home");
            }
        }
        /// <summary>This action used for edit user's details
        [CustomAuth]
        public ActionResult UserEdit(UserRegistration userData)
        {
            try
            {
                if (userData != null)
                {
                    return Json(userLoginBAL.EditUserData(userData));
                }
                return RedirectToAction("ErrorPage", "Home");
            }
            catch (Exception exception)
            {
                //ErrorBAL.ErrorDetailsBAL(userData.Id, exception);
                return RedirectToAction("ErrorPage", "Home");
            }

        }

        /// <summary>This action used for delete user's details
        [CustomAuth]
        public ActionResult UserDelete(int? id)
        {
            try
            {
                if (id != null)
                {
                    return Json(userLoginBAL.DeleteUserData(id));
                }
                return Json("0");
            }
            catch (Exception exception)
            {
                //ErrorBAL.ErrorDetailsBAL(id, exception);
                return RedirectToAction("ErrorPage", "Home");
            }
        }
    }
}
