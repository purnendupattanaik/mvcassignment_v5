﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MindfireSolutions.ViewModels;
using MindfireSolutions.UserModelContext;
using System.Net.Mail;
using System.Net;
using System.Web.Security;
using MindfireSolutions.Models;
using MindfireSolutions.BAL;
using MindfireSolutions.CustomFilters;
using MindfireSolutions.BALInterface;

namespace MindfireSolutions.Controllers
{
    public class LoginController : Controller
    {
        private IUserLoginBAL userLoginBAL;
        private IErrorBAL errorBAL;

        public LoginController(IUserLoginBAL _userLoginBAL, IErrorBAL _errorBAL)
        {
            userLoginBAL = _userLoginBAL;
            errorBAL = _errorBAL;
        }
        // GET: Login
        /// <summary>This action used for user login
        public ActionResult UserLogin()
        {
            return View();
        }

        /// <summary>This action used for user login
        [HttpPost]
        public ActionResult UserLogin(UserLogin userLogin)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(userLogin);
                }
                using (UserAccountContext obj = new UserAccountContext())
                {
                    string pass = PasswordClass.Encrypt(userLogin.Password);
                    var userData = obj.UserDetails.FirstOrDefault(x => x.Email == userLogin.EmailId && x.Password == pass);
                    if (userData != null && userData.RoleId == 1)
                    {
                        FormsAuthentication.SetAuthCookie(userData.Email, false);
                        return RedirectToAction("AdminIndex", "Admin", new { id = userData.Id });
                    }
                    else if (userData != null && userData.RoleId != 1)
                    {
                        FormsAuthentication.SetAuthCookie(userData.Email, false);
                        return RedirectToAction("UserDetails", "Home", new { id = userData.Id });
                    }
                    else
                    {
                        ViewBag.messageLogin = "Invalid email or password";
                    }
                }
            }
            catch (Exception exception)
            {
                using (UserAccountContext obj = new UserAccountContext())
                {
                    string pass = PasswordClass.Encrypt(userLogin.Password);
                    var userData = obj.UserDetails.FirstOrDefault(x => x.Email == userLogin.EmailId && x.Password == pass);
                    errorBAL.ErrorDetailsBAL(userData.Id, exception);
                    return RedirectToAction("ErrorPage", "Home");
                }
            }
            return View();
        }

        /// <summary>This action used for user logout
        public ActionResult UserLogout()
        {
            try
            {
                TempData.Clear();
                Session.Clear();
                FormsAuthentication.SignOut();
                return RedirectToAction("UserLogin", "Login");
            }
            catch (Exception exception)
            {
                return RedirectToAction("ErrorPage", "Home");
            }

        }

        /// <summary>This action used for change password
        [CustomAuth]
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpGet]
        [CustomAuth]

        /// <summary>This action used for change password
        public ActionResult ForgotPassword()
        {
            return View();
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        [CustomAuth]

        /// <summary>This action used for change password
        public ActionResult ForgotPassword(ForgetPasswordModel userAccount)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(userAccount);
                }

                return View(userLoginBAL.ForgetPasswordBAL(userAccount));
            }
            catch (Exception exception)
            {
                return RedirectToAction("ErrorPage", "Home");
            }

        }

        /// <summary>This action used for generate password
        [HttpPost]
        public ActionResult GeneratePassword(UserRegistration userAccount)
        {
            try
            {
                if (userAccount.Code != int.Parse(TempData["code"].ToString()))
                {
                    return RedirectToAction("ForgotPassword", "Login");
                }
                return View();
            }
            catch (Exception exception)
            {
                return RedirectToAction("ErrorPage", "Home");
            }

        }
    }
}