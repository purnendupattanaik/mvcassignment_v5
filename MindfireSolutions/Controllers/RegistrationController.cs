﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MindfireSolutions.ViewModels;
using MindfireSolutions.Models;
using MindfireSolutions.UserModelContext;
using MindfireSolutions.BAL;
using System.Net.Mail;
using System.Net;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using MindfireSolutions.BALInterface;

namespace MindfireSolutions.Controllers
{
    public class RegistrationController : Controller
    {
        private IUserRegisterBAL userRegisterBAL;

        public RegistrationController(IUserRegisterBAL _userRegisterBAL)
        {
            userRegisterBAL = _userRegisterBAL;
        }

        /// <summary>This action used for user register
        // GET: Registration
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>This action used for user register
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Register(UserRegistration userAccount)
        {
            try
            {
                string message = "";
                bool status = false;
                if (!ModelState.IsValid)
                {
                    return View(userAccount);
                }
                using (UserAccountContext userDB = new UserAccountContext())
                {
                    if (userRegisterBAL.IsEmailExistBAl(userAccount) != null)
                    {
                        ViewBag.invalidEmailMessage = "Email id is already Exit";
                        return View(userAccount);
                    }
                    User userModel = userRegisterBAL.UploadUserData(userDB, userAccount);
                    if (userRegisterBAL.ContactValidation(userAccount) != 0)
                    {
                        ViewBag.ContactError = "Invalid input";
                        return View(userAccount);
                    }
                    userRegisterBAL.SendVerificationCode(userModel.Email, userModel.ActivationCode.ToString());
                    message = "Registered successfully done . Account Activation link " +
                        "has been sent to your email id : " + userModel.Email;
                    status = true;
                    ModelState.Clear();

                    ViewBag.successMessage = message;
                    ViewBag.status = status;

                }
                return View(userAccount);
            }
            catch (Exception exception)
            {
                return RedirectToAction("ErrorPage", "Home");
            }
        }
        public ActionResult ValidateEmail()
        {
            return Json(userRegisterBAL.ValidateEmailBAl(Request["email"]));
        }
    }
}
