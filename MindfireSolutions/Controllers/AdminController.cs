﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MindfireSolutions.BAL;
using MindfireSolutions.UserModelContext;
using MindfireSolutions.ViewModels;
using MindfireSolutions.CustomFilters;
using MindfireSolutions.Models;
using MindfireSolutions.BALInterface;
namespace MindfireSolutions.Controllers
{
    public class AdminController : Controller
    {
        private IAdminBAL adminBAL;
        /// <summary>This action is for admin details
        // GET: Admin

        public AdminController(IAdminBAL _adminBAL)
        {
            adminBAL = _adminBAL;
        }
        [CustomAuth]
        public ActionResult AdminIndex(int? id)
        {
            try
            {
                return View(adminBAL.FindAdminDetails(id));
            }
            catch (Exception exception)
            {
                //ErrorBAL.ErrorDetailsBAL(id, exception);
                return RedirectToAction("ErrorPage", "Home");
            }
        }
    }
}