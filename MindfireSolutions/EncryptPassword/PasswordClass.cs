﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MindfireSolutions
{
    public class PasswordClass
    {
        /// <summary>This method is used for encrypt password
        public static string Encrypt(string password)
        {
            UnicodeEncoding unicodeEncode = new UnicodeEncoding();
            byte[] bytPassword = unicodeEncode.GetBytes(password);
            SHA512Managed sha = new SHA512Managed();
            byte[] hash = sha.ComputeHash(bytPassword);
            return Convert.ToBase64String(hash);
        }
    }
}