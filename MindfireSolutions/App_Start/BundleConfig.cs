﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace MindfireSolutions.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").IncludeDirectory("~/scripts/", "*.js", true));
            bundles.Add(new StyleBundle("~/bundles/styles").IncludeDirectory("~/Content/","*.css",true ));
        }
    }
}