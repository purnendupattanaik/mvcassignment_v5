using MindfireSolutions.BAL;
using MindfireSolutions.BALInterface;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace MindfireSolutions
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            container.RegisterType<IAdminBAL, AdminBAL>();
            container.RegisterType<IUserRegisterBAL, UserRegisterBAL>();
            container.RegisterType<IUserLoginBAL, UserLoginBAL>();
            container.RegisterType<IErrorBAL, ErrorBAL>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}