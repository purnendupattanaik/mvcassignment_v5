﻿using MindfireSolutions.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace MindfireSolutions.BALInterface
{

    public interface IUserLoginBAL
    {
        void SendVarificationCode(string emailId, int activationCode);
        string DeleteUserData(int? id);
        string EditUserData(UserRegistration userData);
        UserDetailsView ShowUserData(int? id);
        ForgetPasswordModel ForgetPasswordBAL(ForgetPasswordModel userAccount);
    }
}