﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MindfireSolutions.BALInterface
{
    public interface IErrorBAL
    {
        void ErrorDetailsBAL(int? id, Exception exception);
    }
}