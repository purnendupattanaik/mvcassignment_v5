﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Net;
using System.Net.Mail;
using MindfireSolutions.UserModelContext;
using MindfireSolutions.ViewModels;
using MindfireSolutions.Models;

namespace MindfireSolutions.BALInterface
{
    public interface IUserRegisterBAL
    {
        User UploadUserData(UserAccountContext userDB, UserRegistration userAccount);
        string ValidateEmailBAl(string email);
        User IsEmailExistBAl(UserRegistration userAccount);
        int ContactValidation(UserRegistration userAccount);
        void SendVerificationCode(string emailId, string activationCode);
    }
}