﻿using MindfireSolutions.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace MindfireSolutions.BALInterface
{
    public interface IAdminBAL
    {
        UserDetailsView FindAdminDetails(int? id);
    }
}