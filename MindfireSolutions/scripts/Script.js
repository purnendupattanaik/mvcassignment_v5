﻿var cloneCount = 1;
$(document).ready(function () {
    $("#EditPopUpModal").hide();
    //  var elementGenerator = '<input class="form-control" name="ContactNumber" placeholder="Enter Your Number" type="text" value=""><span class="col-md-8 error"></span>';

    $("#AddContact").click(function () {
        $("#CloneItem").clone().attr('id', '' + cloneCount).append('<input class="form-control contact-class" name="ContactNumber" placeholder="Enter Your Number" type="text" value="" id="AltContactNumbers' + cloneCount + '" oninput="contactValid(' + cloneCount + ')"><span class="col-md-8 error" id="errorMessage' + cloneCount + '"></span>').insertBefore("#AppendItem");
        cloneCount++;
    });
    $("#ContactNumber").on("input", function () {

        var firstContactData = $("#ContactNumber").val();
        var firstError = "#FirstErrorContactNumber";
        checkContact(firstContactData, firstError);
    });
    $("#Password").on("input", function () {
        checkPassword()
    });
});
function checkPassword() {
    var dataId = "#Password";
    $("#ErrorPasswordLength").show();
    $("#ErrorPasswordUppercase").show();
    $("#ErrorPasswordLowerCase").show();
    $("#ErrorPasswordNumber").show();
    $("#ErrorPasswordSymbol").show();
    var uppercaseReg = /[A-Z]/;
    var lowercaseReg = /[a-z]/;
    var numberReg = /[0-9]/;
    var symbolReg = /[#?!@$%^&*-]/;
    var passwordLength = $(dataId).val().length;
    if (passwordLength < 8) {
        $("#ErrorPasswordLength").css("color", "red");
        $("#ErrorPasswordLength").show();
    }
    else {
        $("#ErrorPasswordLength").css("color", "green");
        $("#ErrorPasswordLength").show();
    }
    if (!numberReg.test($(dataId).val())) {
        $("#ErrorPasswordNumber").css("color", "red");
        $("#ErrorPasswordNumber").show();
    }
    else {
        $("#ErrorPasswordNumber").css("color", "green");
        $("#ErrorPasswordNumber").show();
    }
    if (!uppercaseReg.test($(dataId).val())) {
        $("#ErrorPasswordUppercase").css("color", "red");
        $("#ErrorPasswordUppercase").show();
    }
    else {
        $("#ErrorPasswordUppercase").css("color", "green");
        $("#ErrorPasswordUppercase").show();
    }
    if (!lowercaseReg.test($(dataId).val())) {
        $("#ErrorPasswordLowerCase").css("color", "red");
        $("#ErrorPasswordLowerCase").show();
    }
    else {
        $("#ErrorPasswordLowerCase").css("color", "green");
        $("#ErrorPasswordLowerCase").show();
    }
    if (!symbolReg.test($(dataId).val())) {
        $("#ErrorPasswordSymbol").css("color", "red");
        $("#ErrorPasswordSymbol").show();
    }
    else {
        $("#ErrorPasswordSymbol").css("color", "green");
        $("#ErrorPasswordSymbol").show();
    }
}
function saveEditData() {
    var id = $('#IdPopUp').val();
    var fname = $('#FirstNamePopUp').val();
    var lname = $('#LastNamePopUp').val();
    var address = $('#AddressPopUp').val();
    var editData = {
        Id: id,
        FirstName: fname,
        LastName: lname,
        Address: address
    }
    $.ajax({
        type: 'POST',
        url: "/Home/UserEdit",
        data: JSON.stringify(editData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data == "1") {
                alert("Edited successfully");
                location.reload();
                $("#EditPopUpModal").hide();
            }
            else {
                alert("Not edited");
            }

        },
        error: function (a, jqXHR, exception) { }
    })
}
function editUser(data) {
    //var obj = JSON.parse(data);
    $("#EditPopUpModal").show();
    $('#IdPopUp').val(data.Id);
    $('#FirstNamePopUp').val(data.FirstName);
    $('#LastNamePopUp').val(data.LastName);
    $('#AddressPopUp').val(data.Address);
}
function closeUserModal() {
    $("#EditPopUpModal").hide();
}

function userDelete(id) {
    if (confirm("Are you sure?")) {
        $.ajax({
            type: "POST",
            url: "/Home/UserDelete?id=" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response == "1") {
                    alert("Deleted Successfully");
                    location.reload();
                }

            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        })
    }
}

function checkContact(data, error) {
    var regex = /^[0-9]*$/;
    if (!(regex.test(data))) {
        $(error).html("Enter only numbers");
    }
    else {
        $(error).html("");
    }
}
function contactValid(cloneCount) {
    for (var i = 1; i <= cloneCount; i++) {
        var contactValue = $("#AltContactNumbers" + i).val();
        var message = "#errorMessage" + i;
        checkContact(contactValue, message);
    }
}
