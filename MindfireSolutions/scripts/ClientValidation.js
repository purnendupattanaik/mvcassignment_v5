﻿$(function () {
    $("#ErrorEmailId").hide();
    $("#ErrorPasswordLength").hide();
    $("#ErrorPasswordUppercase").hide();
    $("#ErrorPasswordLowerCase").hide();
    $("#ErrorPasswordNumber").hide();
    $("#ErrorPasswordSymbol").hide();
    $("#ErrorEditFirstName").hide();
    $("#ErrorEditLastName").hide();

    $("#EmailId").focusout(function () {
        checkEmail("#EmailId", "#ErrorEmailId");
    });
    $("#FirstNamePopUp").on("input", function () {
        checkName("#FirstNamePopUp", "#ErrorEditFirstName", "first name");
    });
    $("#LastNamePopUp").on("input", function () {
        checkName("#LastNamePopUp", "#ErrorEditLastName", "last name");
    });

    function checkName(dataId, errorId, name) {
        var regex = /^[a-zA-Z]+$/;
        var nameLength = $(dataId).val().length;
        if (nameLength < 1) {
            $(errorId).html("You must provide a " + name);
            $(errorId).show();
        }
        else if (nameLength > 20) {
            $(errorId).html("Should be less than 20 character");
            $(errorId).show();
        }
        else if (!(regex.test($(dataId).val()))) {
            $(errorId).html("Use only alphabets");
            $(errorId).show();
        }
        else {
            $(errorId).hide();
        }
    }

    function checkEmail(dataId, errorId) {
        var regex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        var email = $(dataId).val();
        var emailLength = $(dataId).val().length;
        if (emailLength < 1) {
            $(errorId).html("You must provide a email address");
            $(errorId).show();
        }
        else if (!(regex.test($(dataId).val()))) {
            $(errorId).html("Invalid email");
            $(errorId).show();
        }
        else {
            $(errorId).hide();
        }
        var validemail = new FormData();
        validemail.append("email", email);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "/Registration/ValidateEmail",
            data: validemail,
            //data: JSON.stringify({ 'email' : email}),
            contentType: false,
            processData: false,
            success: function (data) {
                if (data == "1") {
                    $(errorId).html("This email address is already exist");
                    $(errorId).css("color", "red");
                    $(errorId).show();
                }
                else {
                    $(errorId).html("Email is available");
                    $(errorId).show().css("color", "green");
                }
            },
            error: function (a, jqXHR, exception) { }
        });
    }

});