﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MindfireSolutions.UserModelContext;
using MindfireSolutions.ViewModels;
using MindfireSolutions.BALInterface;

namespace MindfireSolutions.BAL
{
    public class AdminBAL :IAdminBAL
    {
        /// <summary>This method used for Find Admin Details
        public  UserDetailsView FindAdminDetails(int? id)
        {
            using (UserAccountContext userAccount = new UserAccountContext())
            {

                var obj = userAccount.UserDetails.Find(id);
                var userContactData = userAccount.RelationshipHolds.Where(m => m.Id == obj.Id).Include(m => m.UserContactData).ToList();
                var data = new UserDetailsView
                {
                    Id = obj.Id,
                    FirstName = obj.FirstName,
                    LastName = obj.LastName,
                    Mobile = obj.Mobile,
                    Email = obj.Email,
                    Address = obj.Address,
                    ImageUrl = obj.ImageUrl,
                    AlternetText = obj.AlternetText,
                    UserContactList = userContactData,
                    UserRoleId = obj.RoleId
                };
                return data;
            }
        }
    }
}