﻿using MindfireSolutions.BAL;
using MindfireSolutions.UserModelContext;
using MindfireSolutions.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using MindfireSolutions.BALInterface;
using System.Data.Entity;

namespace MindfireSolutions.BAL
{

    public class UserLoginBAL: IUserLoginBAL
    {
        /// <summary>This method used for sending of code for forgot password
        public void SendVarificationCode(string emailId, int activationCode)
        {
            var verifyCode = "" + activationCode;
            var fromEmail = new MailAddress("mindfiresolutionspurnendu@gmail.com", "DotNet MVC");
            var toEmail = new MailAddress(emailId);
            var fromEmailPassword = "mindfire@123";
            string body = "Your Code <strong>" + verifyCode + "</strong>";
            string subject = "Change password code";
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }
        /// <summary>This method used for delete data of user
        public string DeleteUserData(int? id)
        {
            using (UserAccountContext userAccount = new UserAccountContext())
            {
                var deleteUser = userAccount.UserDetails.Find(id);
                var editUserData = userAccount.RelationshipHolds.Where(m => m.Id == id);
                foreach (var data in editUserData)
                {
                    userAccount.RelationshipHolds.Remove(data);
                }
                userAccount.UserDetails.Remove(deleteUser);
                userAccount.SaveChanges();
                return "1";
            }
        }
        /// <summary>This method used for edit data of user
        public string EditUserData(UserRegistration userData)
        {
            using (UserAccountContext userAccount = new UserAccountContext())
            {
                var editDetails = userAccount.UserDetails.Find(userData.Id);
                if (editDetails != null)
                {
                    editDetails.FirstName = userData.FirstName;
                    editDetails.LastName = userData.LastName;
                    editDetails.Address = userData.Address;
                    userAccount.SaveChanges();
                    return "1";
                }
                return "0";
            }
        }
        /// <summary>This method used for show user details
        public UserDetailsView ShowUserData(int? id)
        {
            using (UserAccountContext userAccount = new UserAccountContext())
            {
                var obj = userAccount.UserDetails.Find(id);
                var userContactData = userAccount.RelationshipHolds.Where(m => m.Id == obj.Id).Include(m => m.UserContactData).ToList();
                var userDetailsObj = new UserDetailsView
                {
                    Id = obj.Id,
                    FirstName = obj.FirstName,
                    LastName = obj.LastName,
                    Mobile = obj.Mobile,
                    Email = obj.Email,
                    Address = obj.Address,
                    ImageUrl = obj.ImageUrl,
                    AlternetText = obj.AlternetText,
                    UserContactList = userContactData
                };
                return userDetailsObj;
            }
        }
        public ForgetPasswordModel ForgetPasswordBAL(ForgetPasswordModel userAccount)
        {
            string message = "";
            bool status = false;
            Random random = new Random();
            int value = random.Next(10000);
            SendVarificationCode(userAccount.EmailId, value);
            message = "code has been send to your email id : " + userAccount.EmailId;
            status = true;
            //ModelState.Clear();
            //TempData["code"] = value;
            //Session["verifyEmail"] = userAccount.EmailId;
            //ViewBag.successMessage = message;
            //ViewBag.status = status;
            return userAccount;
        }
    }
}