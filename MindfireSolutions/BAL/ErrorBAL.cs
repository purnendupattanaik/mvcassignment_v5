﻿using MindfireSolutions.BAL;
using MindfireSolutions.Models;
using MindfireSolutions.UserModelContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MindfireSolutions.BALInterface;
namespace MindfireSolutions.BAL
{
    public class ErrorBAL :IErrorBAL
    {
        /// <summary>This method used for store error in database
        public void ErrorDetailsBAL(int? id, Exception exception)
        {
            using (UserAccountContext userAccount = new UserAccountContext())
            {
                var UserErrorModel = new ErrorAndExceptionClass
                {
                    ErrorId = id.Value,
                    ErrorName = exception.ToString(),
                    Time = System.DateTime.Now.ToString()
                };
                userAccount.ErrorDetails.Add(UserErrorModel);
                userAccount.SaveChanges();
            }
        }
    }
}