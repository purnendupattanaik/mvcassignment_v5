﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Net;
using System.Net.Mail;
using MindfireSolutions.Models;
using MindfireSolutions.UserModelContext;
using MindfireSolutions.ViewModels;
using System.IO;
using MindfireSolutions.BALInterface;
namespace MindfireSolutions.BAL
{
    public class UserRegisterBAL: IUserRegisterBAL
    {
        ///Here user data are uploaded in side database
        public User UploadUserData(UserAccountContext userDB, UserRegistration userAccount)
        {
            var uploadDir = "~/Images";
            var fileName = userAccount.EmailId + userAccount.ImageUpload.FileName;
            var imagePath = Path.Combine(HttpContext.Current.Server.MapPath(uploadDir), fileName);
            var imageUrl = Path.Combine(uploadDir, fileName);
            userAccount.ImageUpload.SaveAs(imagePath);
            var userModel = new User
            {
                ActivationCode = Guid.NewGuid(),
                ImageUrl = imageUrl,
                FirstName = userAccount.FirstName,
                LastName = userAccount.LastName,
                Password = PasswordClass.Encrypt(userAccount.Password),
                Email = userAccount.EmailId,
                Address = userAccount.Address,
                Mobile = userAccount.Mobile,
                AlternetText = userAccount.FirstName + " " + userAccount.LastName + "'s Photo",
                RoleId = 2
            };
            userDB.UserDetails.Add(userModel);
            userDB.SaveChanges();
            for (int counter = 0; counter < userAccount.Contact.Count(); counter++)
            {
                var userContactModel = new UserContact
                {
                    ContactNumber = userAccount.ContactNumber[counter],
                    ContactId = (int)userAccount.Contact[counter],
                    Id = userModel.Id
                };
                userDB.RelationshipHolds.Add(userContactModel);
            }
            userDB.SaveChanges();
            return userModel;
        }
        /// <summary>This method used for client side email validation
        public string ValidateEmailBAl(string email)
        {
            using (UserAccountContext userDB = new UserAccountContext())
            {
                var isExistEmail = userDB.UserDetails.Any(x => x.Email == email);
                if (isExistEmail)
                {
                    return "1";
                }
                return "0";
            }
        }
        /// <summary>This method used for server side email validation
        public User IsEmailExistBAl(UserRegistration userAccount)
        {
            using (UserAccountContext userDB = new UserAccountContext())
            {
                var userExist = userDB.UserDetails.FirstOrDefault(x => x.Email == userAccount.EmailId);
                return userExist;
            }
        }

        /// <summary>This method used for server side contact validation
        public int ContactValidation(UserRegistration userAccount)
        {
            Regex rx = new Regex(@"^[0-9]*$");
            for (int counter = 0; counter < userAccount.Contact.Count(); counter++)
            {
                if (!(rx.IsMatch(userAccount.ContactNumber[counter])))
                {
                    return 1;
                }
            }
            return 0;
        }
        /// <summary>This method used for sending verification message
        public void SendVerificationCode(string emailId, string activationCode)
        {
            var verifyUrl = "/Login/UserLogin" + activationCode;
            //var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);
            var link = "activated";
            var fromEmail = new MailAddress("mindfiresolutionspurnendu@gmail.com", "DotNet MVC");
            var toEmail = new MailAddress(emailId);
            var fromEmailPassword = "mindfire@123";
            string subject = "Your account is successfully created";
            string body = "<br/><br/>We are excited to tell you that your MVC account is" +
                "successfully created . Please click on below link to verify your account" +
                "<br/><br/><a href = '" + link + "'>" + link + "</a>";
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

    }
}